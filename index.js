// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
daftarHewan.forEach(function(item){
    console.log(item)
})

console.log('\n')

// Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

function introduce() {
    return "Nama saya " + data.name + " umur saya " + data.age + " alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby
  }
var perkenalan = introduce(data)
console.log(perkenalan)

console.log('\n')

// Soal 3

function hitung_huruf_vokal(kata)
{
    const count = kata.match(/[aeiou]/gi).length
    return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)


console.log('\n')


// Soal 4

function hitung(angka)
{
    var count = angka * 2 - 2
    return count
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8